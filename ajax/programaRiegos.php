<?php
include("conexion.php");
function dateDiff($start, $end) {
    $start_ts = strtotime($start);
    $end_ts = strtotime($end);
    $diff = $end_ts - $start_ts;
    return $diff;
}
function saber_dia($nombredia) {
    //$dias = array('', 'Lunes','Martes','Miercoles','Jueves','Viernes','Sabado', 'Domingo');
    $fecha = date('N', strtotime($nombredia));
    $fecha = ($fecha - 1) * 86400;
    return $fecha;
}
$old_time = date('2009-10-11 19:10');
$young_time = date('3020-10-11 19:10');
$id_planificaciones_riegos  = $_POST['id_planificaciones_riegos'];
$tipo_plafinicacion         = $_POST['tipo_plafinicacion'];
$fecha_ini_planificacion    = $_POST['fecha_ini_planificacion'];
$repeticiones               = $_POST['repeticiones'];
$planificacion = array();
$riegos = array();
$x=1;
$i=0;
$sql = "select * from planificaciones_riegos where id_planificacion = $id_planificaciones_riegos";
$consulta = mysql_query($sql, $link);
while ($datatmp = mysql_fetch_assoc($consulta)){ 
    array_push($planificacion,$datatmp);
    if($old_time < $datatmp['fecha_fin']){
        $old_time = $datatmp['fecha_fin'];
    }
    if($young_time > $datatmp['fecha_ini']){
       $young_time = $datatmp['fecha_ini'];
    }    
}
$distancia_dias = saber_dia($young_time);
if($tipo_plafinicacion == 2){
    $date_fin_iteracion = date('Y-m-d',strtotime($old_time));
    $date_ini_iteracion = date('Y-m-d',strtotime($young_time));
    $date_fin_iteracion = date('Y-m-d H:i:s', (strtotime($date_fin_iteracion ) + 86399));
    $duracion_riegos =  strtotime($date_fin_iteracion) - strtotime($young_time);
    $duracion_riegos = ceil($duracion_riegos/86399);
    if($duracion_riegos == 0){$duracion_riegos = 1;}
    $diff_fecha = strtotime($fecha_ini_planificacion) - (strtotime($date_ini_iteracion));
}else{
    $date_ini_iteracion = date('Y-m-d',strtotime($young_time));
    $diff_fecha = strtotime($fecha_ini_planificacion) - (strtotime($date_ini_iteracion)) + $distancia_dias;
}
$iteracion = 0;
while($x <= $repeticiones){
    foreach ($planificacion as &$valor) {
        $id_sector = $valor['id_sector'];
        $sql = "select nombre_sector from control_riego where  id_sector = $id_sector";
        $consulta2 = mysql_query($sql, $link);
        if($datatmp2 = mysql_fetch_array($consulta2)){
            $nombre_sector = $datatmp2['nombre_sector'];
        }
        $date_ini = date('Y-m-d',strtotime($valor['fecha_ini']));
        $riegos[$i]['tipo'] = "A";
        $riegos[$i]['update_p'] = 0;
        $riegos[$i]['id_subriego'] = 0;
        $riegos[$i]['estado'] = 0;
        $riegos[$i]['id_programacion_riego'] = $i; 
        $riegos[$i]['sector'] = $nombre_sector;
        if($tipo_plafinicacion == 1){
            $riegos[$i]['p_fecha_ini'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_ini']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['p_fecha_fin'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_fin']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['r_fecha_ini'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_ini']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['r_fecha_fin'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_fin']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['numero_filas'] = 0;
        }else if($tipo_plafinicacion == 2){
            $riegos[$i]['p_fecha_ini'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_ini']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['p_fecha_fin'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_fin']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['r_fecha_ini'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_ini']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['r_fecha_fin'] = date('Y-m-d H:i:s', (strtotime($valor['fecha_fin']) + ($diff_fecha + $iteracion) ));
            $riegos[$i]['numero_filas'] = 0;
        }
        $i++;
    }
    if($tipo_plafinicacion == 1){
        $iteracion = $iteracion + 604800;
    }else if($tipo_plafinicacion == 2){
        $iteracion = $x * ($duracion_riegos * 86400);
    }
    $x++;
}

echo json_encode($riegos);
?>
