<?php
include("conexion.php");
$sql = "select estado,id_programacion_riego,sector,p_fecha_ini,p_fecha_fin,r_fecha_ini,r_fecha_fin from programacion";
$consulta = mysql_query($sql, $link);
$i = 0;

while ($datatmp = mysql_fetch_array($consulta)) {
    $nombre_sector[$i] = $datatmp['sector'];
    $p_fecha_ini[$i] = $datatmp['p_fecha_ini'];
    $p_fecha_fin[$i] = $datatmp['p_fecha_fin'];
    $r_fecha_ini[$i] = $datatmp['r_fecha_ini'];
    $r_fecha_fin[$i] = $datatmp['r_fecha_fin'];
    $estado[$i] = $datatmp['estado'];
    $id[$i] = $datatmp['id_programacion_riego'];
    $i++; 
}
$id_empresa=1;
if(isset($id_empresa)){
$sql = "select id_sector,nombre_sector from control_riego where id_empresa =$id_empresa group by nombre_sector";
$consulta = mysql_query($sql, $link);
$largo = mysql_num_rows($consulta);
$i = 0;

while ($datatmp = mysql_fetch_array($consulta)) {
    $id_sector[$i] = $datatmp['id_sector'];
    $nombre_sector_control[$i] = $datatmp['nombre_sector'];
    $i++; 
}
}
?>

<html>
    <head>
        <title>Timeline mobile demo</title>

        <!-- for mobile devices like android and iphone -->
        <meta charset="utf-8">
        <meta content="True" name="HandheldFriendly" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

        <script type="text/javascript" src="http://www.google.com/jsapi"></script>
        <script type="text/javascript" src="timeline/timeline.js"></script>
        <script type="text/javascript" src="timeline/timeline-locales.js"></script>
        <script src="../lib/moment.js"></script>
        <script src="../lib/es.js"></script>
        <script src="../lib/bootstrap-datetimepicker.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="../lib/sweet-alert.js"></script> 
        <link rel="stylesheet" type="text/css" href="../lib/sweet-alert.css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../lib/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../timeline.css">
        <link rel="stylesheet" href="../nav.css">
        <style>
            div.timeline-event {
                border-width: 4px;
          
              }
           
            div.timeline-event-selected {
                border-color: #2a0bb4;
            }
            #bodypage {
                -ms-user-select:none;
                font-family:Segoe UI,Segoe,Tahoma,Arial,Verdana,sans-serif;
                margin-left: 0px;
                margin-right: 0px;
                margin-top: 0px;
                margin-bottom: 0px;
            }
            header#colophon {
                position: absolute;
                top: 0px;
                display: block;
                width: 99.7%;
                height: 3.7%;
                text-align: center;
                background: linear-gradient(#3c3c3c,#111);
                color: white;
                z-index:4;
                padding-top: 0.3%;
                padding-left: 0.3%;
            }
            #titulo{
                height:8%;
                overflow-y: hidden;
                overflow-x: hidden;
            }

            #abuelo {
                width:100%;
                height:96.3%;
                margin: auto;
                position: absolute;
                overflow-y: hidden;
                overflow-x: hidden;
                top: 3.7%;
            }
            #mytimeline {
                width:100%;
                height:92%;
                margin: auto;
                position: absolute;
                overflow-y: hidden;
                overflow-x: hidden;
            }
        </style>
        <script>
		    var nombre_sector_control = <?php echo json_encode($nombre_sector_control); ?>;
			var names = <?php echo json_encode($nombre_sector); ?>;
            var id_sector = <?php echo json_encode($id_sector); ?>;
            var p_fecha_ini =<?php echo json_encode($p_fecha_ini); ?>;
            var p_fecha_fin =<?php echo json_encode($p_fecha_fin); ?>;
            var r_fecha_ini =<?php echo json_encode($r_fecha_ini); ?>;
            var r_fecha_fin =<?php echo json_encode($r_fecha_fin); ?>;
            var estado = <?php echo json_encode($estado); ?>;
            var id =<?php echo json_encode($id); ?>;
            var timeline = undefined;
            var data = undefined;
            var id_sectorControl = 0;
			var nombre;
            google.load("visualization", "1");

            // Set callback to run when API is loaded
            google.setOnLoadCallback(drawVisualization);

            // Called when the Visualization API is loaded.
            function drawVisualization() {
                // Create and populate a data table.
                data = new google.visualization.DataTable();
                data.addColumn('datetime', 'start');
                data.addColumn('datetime', 'end');
                data.addColumn('string', 'content');
                data.addColumn('string', 'group');
                data.addColumn('string', 'className');

                // create some random data
                
                var largo = fecha_ini.length;
                for (var n = 0, len = p_fecha_ini.length; n < len; n++) {
                    var name = names[n];
                    var now = new Date();
                    var end = new Date();
                    var est = estado[n];

                    var start = new Date(p_fecha_ini[n]);
                    var end = new Date(p_fecha_fin[n]);
                    var duracion = end - start;
                    duracion = (((duracion / 1000) / 60)/60);
                    var start2 = new Date(r_fecha_ini[n]);
                    var end2 = new Date(r_fecha_fin[n]);
                    var duracion2 = end2 - start2;
                    duracion2 = (((duracion2 / 1000) / 60 )/60);
                    
                    duracion = duracion * 53.666;
                 					
					var ancho = duracion2 * 53.666;
                    var ancho = ancho *100;
                    
					var ancho = ancho/duracion;
                    
                    

                    var ini = p_fecha_ini[n].split(" ");
                    var fin = p_fecha_fin[n].split(" ");
                    if (est == 4){
                        var color = 'rgb(195, 49, 45)';
                    }else{var color = 'rgb(0, 119, 255)';}
                    var availability = '<div style="border-width:4px;" onMouseOver=creainfo("' + n + '"); onMouseOut=borrarinfo(); ><div style=" width: ' + ancho + '%; background-color: '+color+'; "id="i'+n+'";><div style="color: #5D99C3;">&nbsp;</div></div></div>';
                    var group = availability.toLowerCase();
                    var content = availability;
                    data.addRow([start, end, content, name, group]);

  

                }

                // specify options
                var options = {
                    width: "100%",
                    height: "99%",
                    layout: "box",
                    axisOnTop: true,
                    eventMargin: 10, // minimal margin between events
                    eventMarginAxis: 0, // minimal margin beteen events and the axis
                    editable: false,
                    showNavigation: true,
                    selectable: true,
                    locale: 'es',
                    groupsChangeable: false,
                    groupMinHeight: 40,
                    zoomable: false

                };

                // Instantiate our timeline object.
                timeline = new links.Timeline(document.getElementById('mytimeline'), options);

                // register event listeners
                google.visualization.events.addListener(timeline, 'edit', onEdit);

                // Draw our timeline with the created data and options
                timeline.draw(data);

                // Set a customized visible range
                var start = new Date(now.getTime() - 12 * 60 * 60 * 1000);
                var end = new Date(now.getTime() + 12 * 60 * 60 * 1000);
                timeline.setVisibleChartRange(start, end);
            }
           
            function getSelectedRow() {
                var row = undefined;
                var sel = timeline.getSelection();
                if (sel.length) {
                    if (sel[0].row != undefined) {
                        row = sel[0].row;
                    }
                }
                return row;
            }
            function obtenersector(id){
				id_sectorControl=id_sector[id];
				nombre=nombre_sector_control[id];
			}
            function strip(html)
            {
                //var tmp = document.createElement("DIV");
                //tmp.innerHTML = html;
                //return tmp.textContent || tmp.innerText;
            }

            // Make a callback function for the select event
            var onEdit = function (event) {
                var row = getSelectedRow();
                var content = data.getValue(row, 2);
                var availability = strip(content);
               
                var newAvailability = prompt("Enter status\n\n" +
                        "Choose from: Available, Unavailable, Maybe", availability);
                if (newAvailability != undefined) {
                    var newContent = newAvailability;
                    data.setValue(row, 2, newContent);
                    data.setValue(row, 4, newAvailability.toLowerCase());
                    timeline.draw(data);
                }
            };

            var onNew = function () {

            };
            function addriego() {
                
        
                var tipo = $('input:radio[name=tipo]:checked').val()
                var fecha_ini = $('#fecha_ini').val()
                var sector = nombre;
                if (tipo == 1) {
                    var fecha_fin = $('#fecha_fin').val();
                    var largo = names.length;
                    largo = largo+1;
                    names[largo] = sector;
                    p_fecha_ini[largo] =fecha_ini;
                    p_fecha_fin[largo] =fecha_fin;
                    r_fecha_ini[largo] =fecha_ini;
                    r_fecha_fin[largo] =fecha_ini;
                    var name = sector;
                    var start = new Date(fecha_ini);
                    var end = new Date(fecha_fin);
                    var availability = '<div onMouseOver=creainfo("' + largo + '"); onMouseOut=borrarinfo(); ><div style=" width: ' + 0 + '%; background-color: rgb(0, 119, 255); border-width:4px;" id="i'+largo+'";><div  style="color: #5D99C3;">&nbsp;</div></div></div>';                    
                    var group = availability.toLowerCase();
                    var content = availability;
                    timeline.addItem({
                        'start': start,
                        'end': end,
                        'name': name,
                        'group': name,
                        'content': content

                    });

                    var count = data.getNumberOfRows();
                    timeline.setSelection([{
                            'row': count - 1
                        }]);
                }
            }
            function eliminarRiego(id) {
              
                document.getElementById("id_riego").value = id;
            }
            function entra1() {
                document.getElementById("fechafin").style.display = "block";
                document.getElementById("volumen").style.display = "none";
                document.getElementById("duracion").style.display = "none";
            }
            function entra2() {
                document.getElementById("fechafin").style.display = "none";
                document.getElementById("volumen").style.display = "block";
                document.getElementById("duracion").style.display = "none";
            }
            function entra3() {
                document.getElementById("fechafin").style.display = "none";
                document.getElementById("volumen").style.display = "none";
                document.getElementById("duracion").style.display = "block";
            }
            function verfertil() {
                document.getElementById("fertil").style.display = "block";
                document.getElementById("nuevo").style.display = "none";
            }
            function entra4() {
                document.getElementById("fechafin2").style.display = "block";
                document.getElementById("volumen2").style.display = "none";
                document.getElementById("duracion2").style.display = "none";
            }
            function entra5() {
                document.getElementById("fechafin2").style.display = "none";
                document.getElementById("volumen2").style.display = "block";
                document.getElementById("duracion2").style.display = "none";
            }
            function entra6() {
                document.getElementById("fechafin2").style.display = "none";
                document.getElementById("volumen2").style.display = "none";
                document.getElementById("duracion2").style.display = "block";
            }
            function borrarinfo() {

                $("#info").remove();
                document.getElementById("mytimeline").style.zIndex = '0';
            }
            function creainfo(indice) {

                var desde = p_fecha_ini[indice];
                var hasta = p_fecha_fin[indice];
                var desdeP = new Date(p_fecha_ini[indice]);
                var hastaP = new Date(p_fecha_fin[indice]);
                var duracion = hastaP - desdeP;
                duracion = ((duracion / 1000) / 60);
                duracion = Math.round(duracion);
                var ini = desde.split(" ");

                var fin = hasta.split(" ");
                var start2 = new Date(r_fecha_ini[indice]);
                var end2 = new Date(r_fecha_fin[indice]);
                var duracion2 = end2 - start2;
                duracion2 = ((duracion2 / 1000) / 60);
                var duracionReal = Math.round(duracion2)
                $("#abuelo").append('<div id="info"><strong>Informacion</strong></br><strong>Inicio:</strong>' + ini[1] + '</br><Strong>Fin:</strong>' + fin[1] + '</br><strong>Tiempo Riego Total:</strong>' + duracion + '&nbsp; min</br><strong>Lleva regando:</strong>' + duracionReal + '&nbsp;min</div>');
                document.captureEvents(Event.MOUSEMOVE);
                var tempX = event.pageX;
                var tempY = event.pageY;
                var tempY = tempY - 70;
                document.getElementById('info').style.marginTop = (tempY) + "px";
                document.getElementById('info').style.marginLeft = (tempX) + "px";
                document.getElementById("info").style.width = '200px';
                document.getElementById("info").style.height = '100px';
                document.getElementById("info").style.backgroundColor = 'white';
                document.getElementById("info").style.borderStyle = 'solid';
                document.getElementById("info").style.borderColor = 'rgb(0, 119, 255);';
                document.getElementById("info").style.borderWidth = '1px';
                document.getElementById("mytimeline").style.zIndex = '-1';




            }
            function setTime() {
            var fecha = document.getElementById('fecha').value;
			var fechaini = fecha + " 00:00:00"
			var fechafin = fecha + " 23:59:59"
			var fechaini = new Date(fechaini);
			var fechafin = new Date(fechafin);
            timeline.setVisibleChartRange(fechaini, fechafin);
            }
            function doDelete() {
            
            
            var sel = timeline.getSelection();
            if (sel.length) {
                if (sel[0].row != undefined) {
                    var row = sel[0].row;
                }
            }
            var idr = id[row];               
            var now = new Date();
            var inir = new Date(p_fecha_ini[row]);
            var finr = new Date(p_fecha_fin[row]);
            if(now > inir && now < finr ){
               
               swal({   title: "¿Estas seguro?",   text: "El riego que ha seleccionado sera re-agendado para que finalice en este momento!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Si, re-agendarlo!",   cancelButtonText: "No, cancelar!",   closeOnConfirm: false,   closeOnCancel: false }, 
               
               function(isConfirm){   
                   if (isConfirm) {
                       document.getElementById('i'+row).style.backgroundColor = 'rgb(195, 49, 45)';   
                       var data = 'id='+idr;
					   
                       $.ajax({
                        url:'updateRiego.php',
                        type: 'post',
                        data: data,
                        beforeSend: function(){
                            console.log('enviando datos a la bd....')
						},
                        success: function(resp){
                           console.log(resp)
                           
                        }

                        })
                       
                       
                       swal("Aceptado!", "Su riego ha sido re-agenado para finalizar en este momento.", "success");   
                   }else{
                       swal("Cancelado!", "Su riego no ha sido re-agenado para finalizar en este momento.", "error");   
                   } 
               });
            }
            else if(now > inir && now > finr ){
            sweetAlert("Oops...", "No puede cancelar un riego que ya se realizo", "error");
            }else{
               swal({   title: "¿Estas seguro?",   text: "El riego que ha seleccionado sera cancelado!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Si, cancelar!",   cancelButtonText: "No, cancelar!",   closeOnConfirm: false,   closeOnCancel: false }, 
               function(isConfirm){   
                   if (isConfirm) {
                       swal("Eliminado!", "Su riego ha sido cancelado.", "success"); 
                       var data = 'id='+idr;
                       $.ajax({
                        url:'deleteRiego.php',
                        type: 'post',
                        data: data,
                        beforeSend: function(){
                            console.log('enviando datos a la bd....')

                        },
                        success: function(resp){
                           console.log(resp)
                           timeline.deleteItem(row); 
                        }

                        })
                   }else{
                       swal("Cancelado!", "Su riego no ha sido eliminado.", "error");   
                   } 
               });
            }
            
            if (row != undefined) {
               // timeline.deleteItem(row);
            } else {
                sweetAlert("Error", "Debe seleccionar un Riego", "error");
            }
        }
        </script>
        <script>
            $(function () {
                $('#datetimepicker2').datetimepicker({
                    locale: 'es',
                    format: 'YYYY-MM-DD LT'
                });
                $('#datetimepicker3').datetimepicker({
                    locale: 'es',
                    format: 'YYYY-MM-DD LT'
                });
                $('#datetimepicker4').datetimepicker({
                    locale: 'es',
                    format: 'YYYY-MM-DD LT'
                });
                $('#datetimepicker5').datetimepicker({
                    locale: 'es',
                    format: 'YYYY-MM-DD LT'
                });
                $('#datetimepicker6').datetimepicker({
                    locale: 'es',
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
        <style type="text/css">
            /* Styles for the page */
            html, body {
                font: 10pt arial;
            }

            #mytimeline {
            }

            #new {
                position: absolute;
                left: 25px;


                text-transform: uppercase;
                color: white;
                font-weight: bold;
                font-size: 40px;
                text-decoration: none;
            }
            .checkbox label, .radio label {
                min-height: 20px;
                margin-bottom: 0;
                font-weight: 400;
                cursor: pointer;
                margin-left: -45%;
            }

            /* Custom styles for the Timeline */
            div.timeline-frame {
                border-color: #5D99C3;

                border-radius: 5px;
                -moz-border-radius: 5px; /* For Firefox 3.6 and older */
            }
            div.timeline-axis {
                border-color: #136e13;

                background-color: #1AA11A;
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#47D26A', endColorstr='#47BD65'); /* for IE */
                background: -webkit-gradient(linear, left top, left bottom, from(#47D26A), to(#47BD65)); /* for webkit browsers */
                background: -moz-linear-gradient(top,  #47D26A,  #47BD65); /* for firefox 3.6+ */
            }
            div.timeline-axis-grid {
            }
            div.timeline-groups-axis {
                border-color: #5D99C3;
            }
            div.timeline-axis-text {
                color: white;
            }
            div.timeline-groups-text {
                color: #4D4D4D;
            }
            div.timeline-event {
                color: black;
                border-radius: 5px;
                background-color: #5D99C3;
            }
            div.timeline-event-content {
                padding: 5px;

            }

            div.unavailable {
                background-color: #F03030; /* red */
                border-color: #bd2828;     /* red */
            }
            div.available {
                background-color: #1AA11A; /* green */
                border-color: #136e13;     /* green */
            }
            div.maybe {
                background-color: #FFA500; /* orange */
                border-color: #cc8100;     /* orange */
            }
            div.timeline-event-content {
                margin: 0px;
                overflow: hidden;
                white-space: nowrap;
                margin: 0;
                padding:0;
            }
        </style>

        <script type="text/javascript">
           
        </script>
    </head>

    <body id="bodypage" onresize="timeline.redraw();">
        <header id="colophon">

            <label style="margin-top: -0.3%;margin-bottom: 0%;font-family:Segoe UI,Segoe,Tahoma,Arial,Verdana,sans-serif;">Lem Control</label>
            <a href="" class="classmenucerrar square green effect-2" data-role="button" data-theme="a" data-icon="delete" onclick="window.close();">Cerrar</a>
            <div class="fit-content" style="margin-top: 0.4%;">
                <ul name="select-choice-1" class="fit-content" id="select-choice-1" style="display: none">
                    <li>
                        <form action="riego.php" method="Post" name="myForm" data-ajax="false" style="margin-top:3px;">
                            <input name="radio-view" id="radio-view-a" type="radio" data-theme="a" data-mini="true" onClick="replotgraficosmes(1)" checked value="Mes"/><label for="radio-view-a">Mes</label>	
                            <input name="radio-view" id="radio-view-b" type="radio" data-theme="a" data-mini="true" onClick="replotgraficossemana()" value="Semana"/><label for="radio-view-b">Semana</label>
                            <input name="radio-view" id="radio-view-c" type="radio" data-theme="a" data-mini="true" onClick="replotgraficosdia()" value="Dia"/><label for="radio-view-c">Dia</label>	
                        </form>
                    </li><li style="margin-top: 0.3%;"><label>|</label></li>
                    <li style="margin-top: 0.2%;">
                        <form action="riego.php" method="Post" name="myForm2" data-ajax="false">
                            <div style="float: left;">
                                <label for="date-1" >Desde:</label>
                                <input type="date" data-mini="true" data-clear-btn="true" class="click" name="date-1" id="date-1" value="<?php echo $fechaini_box ?>" max="<?php echo date('Y-m-d'); ?>" required>   
                                <label for="date-2" >Hasta:</label>
                                <input type="date" data-mini="true" data-clear-btn="true" name="date-2" id="date-2" onclick="fechamin();" value="<?php echo $fechafin_box ?>" max="<?php echo date('Y-m-d'); ?>" required>
                                <input name="id_nodo" id="idnodo" type="hidden" value="<?php echo $id_nodo ?>" visible="false">
                                <input name="unidad" id="unidad" type="hidden" value="<?php echo $unidad ?>" visible="false">
                                <input name="idsensor" id="idsensor" type="hidden" value="<?php echo $id_sensor ?>" visible="false">
                                <input name="subtipo" id="subtipo" type="hidden" value="<?php echo $subtipo ?>" visible="false">
                                <input name="nombresector" id="nombresector" type="hidden" value="<?php echo $nombre_sector ?>" visible="false">			
                            </div>
                            <div style="float: right; margin-left: 15px;">
                                <input type="submit" style="float: none;font-size: initial;"  class="classmenucerrar square green effect-2"  data-inline="true" value="Consultar" />								
                            </div>
                        </form>
                    </li><li style="margin-top: 0.3%;"><label>|</label></li>
                    <li >
                        <input type="button" value="Ingresos" class="classmenucerrar square green effect-2" style="font-size: initial;margin-top:3px;margin-left:10px;" onclick="abrir(1);"/>
                        <input type="button" value="Parametros" class="classmenucerrar square green effect-2" style="font-size: initial;margin-top:3px;" onclick="abrir(2);"()>
                    </li>
                </ul>
            </div>
            <!--a href="" class="cerrar" data-role="button" data-theme="a" data-icon="delete" onclick="window.close();">Cerrar</a-->
        </header>
        <div id="abuelo" style="text-align:center">

            <div style="z-index:200;" id="flotante"></div>


            <div id="titulo" style="text-align:center;">
                <div style="float: left;padding: 15px;width: 230px;">
                    <img src="../img/blanco2.png" alt="" style="width: 220px;">
                </div>
                <div style="float: left;margin-top: 14px;">
                    <label style="margin: 15px;position: relative;top: -23px;font-size: 29px;color: rgb(0, 202, 219);">|</label>
                    <label style="font-family: Segoe UI,Segoe,Tahoma,Arial,Verdana,sans-serif;margin-left: -8px;color: rgb(148, 148, 148);position: relative;top: -24px;">Planificación Riego</label>
                </div>
            </div>
            <div id="mytimeline" lass="padre" style="text-align:center;"></div>
            <a id="new" data-toggle='modal' data-target='#editarSensorTRB' title="Add new status event" href="javascript:void(0);" onclick="onNew();">+</a>
            <div style="float: right;margin-top: -3%;">
                <div class="col-sm-10">

					<input type="button" value="Cancelar" title="Delete selected event" style="margin-left:-40%; float:left;" onclick="doDelete();" class="btn btn-danger">
                    <div class='input-group date' id='datetimepicker6' style="width:180px; "><input type='text' id="fecha"; class="form-control" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                <input type="button" value="Consultar" style="margin-right: -57px; float: right;  margin-top: -16%;" onclick="setTime();" class="btn btn-info">
				</div>
            </div>


            <div class="modal fade" id="editarSensorTRB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4>Agregar Riego</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">*Fecha y Hora de inicio</label>
                                    <div class="col-sm-10">
                                        <div class='input-group date' id='datetimepicker2' style="width:180px">
                                            <input type='text' id="fecha_ini" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">*Tipo de Riego</label>
                                    <div class="col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <input onclick="entra1();" type="radio" name="tipo" value="1" /> Fecha y Hora Fin <input onclick="entra2();" type="radio" name="tipo" value="2" /> Volumen <input onclick="entra3();" type="radio" name="tipo" value="3" /> Duracíon
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;" id="fechafin">
                                    <label for="inputEmail3" class="col-sm-2 control-label">*Fecha y Hora de Fin</label>
                                    <div class="col-sm-10">
                                        <div class='input-group date' id='datetimepicker3' style="width:180px">
                                            <input type='text' id="fecha_fin" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display:none;" id="volumen">
                                    <div>
                                        <label for="inputPassword3" class="col-sm-2 control-label">Volumen:</label>
                                        <div class="col-sm-10">
                                            <div class='input-group date' id='datetimepicker2' style="width:180px">
                                                <input type='text' class="form-control" />                   
                                            </div>
                                        </div>
                                    </div>   

                                </div>
                                <div class="form-group" style="display:none;" id="duracion">
                                    <div>
                                        <label for="inputPassword3" class="col-sm-2 control-label">Duracíon:</label>
                                        <div class="col-sm-10">
                                            <div class='input-group date' id='datetimepicker2' style="width:180px">
                                                <input type='text' class="form-control" />                   
                                            </div>
                                        </div> 
                                    </div> 
                                </div>
                                <div class="form-group" style="display:block;" id="duracion">
                                    <div>
                                        <label for="inputPassword3" class="col-sm-2 control-label">Sector</label>
                                        <div class="col-sm-10">
                                            <div class='input-group date' style="width:180px">
                                                <select onChange="obtenersector(value);">
												<?php
												 $b = 0;
												 while ($b < $largo) {
												   echo '<option  value="'.$b.'">'. $nombre_sector_control[$b] . ' </option>';
												   $b=$b+1;
												}?>
												</select>											
                                            </div>
                                        </div> 
                                    </div> 
                                </div>
                                <div class="form-group" style="display:block;   margin-left: 8%;">

                                </div>
                            </form> 
                            <form class="form-horizontal">          



                                <input style="  margin-left: -38%;" id="nuevo" class="btn btn-default" type="button" value="Agregar Fertirl Riego" onclick="verfertil();">
                                <div id="fertil" style="display:none;">  
                                    <label style="  margin-left: -38%;">Fertirriego</label>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">*Fecha y Hora de inicio</label>
                                        <div class="col-sm-10">
                                            <div class='input-group date' id='datetimepicker4' style="width:180px">
                                                <input type='text' class="form-control" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">*Tipo de Riego</label>
                                        <div class="col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input onclick="entra4();" type="radio" name="tipo" value="4" /> Fecha y Hora Fin <input onclick="entra5();" type="radio" name="tipo" value="5" /> Volumen <input onclick="entra6();" type="radio" name="tipo" value="6" /> Duracíon
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="display:none;" id="fechafin2">
                                        <label for="inputEmail3" class="col-sm-2 control-label">*Fecha y Hora de Fin</label>
                                        <div class="col-sm-10">
                                            <div class='input-group date' id='datetimepicker5' style="width:180px">
                                                <input type='text' class="form-control" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="display:none;" id="volumen2">
                                        <div>
                                            <label for="inputPassword3" class="col-sm-2 control-label">Volumen:</label>
                                            <div class="col-sm-10">
                                                <div class='input-group date' id='datetimepicker2' style="width:180px">
                                                    <input type='text' class="form-control" />                   
                                                </div>
                                            </div>
                                        </div>   

                                    </div>
                                    <div class="form-group" style="display:none;" id="duracion2">
                                        <div>
                                            <label for="inputPassword3" class="col-sm-2 control-label">Duracíon:</label>
                                            <div class="col-sm-10">
                                                <div class='input-group date' id='datetimepicker2' style="width:180px">
                                                    <input type='text' class="form-control" />                   
                                                </div>
                                            </div> 
                                        </div> 
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <input type="button" value="Aceptar" class="insetarTareaRiego " data-dismiss="modal"/>
                        </div>
                    </div>
                </div>   
            </div>



        </div>











    </body>
    <script>
        $(document).ready(function () {

            $(".insetarTareaRiego").click(function () {

                var tipo = $('input:radio[name=tipo]:checked').val()
                var fecha_ini = $('#fecha_ini').val()
  
                if (tipo == 1) {
                    var fecha_fin = $('#fecha_fin').val()

                    var data = 'fecha_ini=' + fecha_ini + '&fecha_fin=' + fecha_fin + '&sector=' + id_sectorControl;
                    $.ajax({
                        url: 'insertarRiegoProgra.php',
                        type: 'post',
                        data: data,
                        beforeSend: function () {
                            console.log('enviando datos a la bd....')
                        },
                        success: function (resp) {
                            console.log(resp)
                            addriego();

                        }
                    })
                }


            });
        });
    </script>
</html>


